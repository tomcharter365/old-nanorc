#!/bin/bash

# =====================================
# Variables
# =====================================

## Questions
oldnano1="$(printf "  \e[0;35mDo you want to install old-nanorc, which will provide syntax highlighting for nano on systems where scopatzs nanorc will not work?\e[0m\n")
$(printf "\n    \e[0;34mFor more info on old-nanorc, go here: https://gitlab.com/tomcharter365/old-nanorc/tree/master/nanorc \e[0m\n\n")
$(printf "\n  [Y/n]: ")"
oldnano2="$(printf "  \e[0;35mWill you use a dark background color for your shell?\e[0m [Y/n]: \n")"

# =====================================
# Functions
# =====================================

ask_q_oldnano2() {
  echo
  read -r -p "${oldnano2}" response
  echo
  if [[ "${response}" =~ ^([yY][eE][sS]|[yY])+$ ]] ; then
    # answered yes
    # optional color changes for nano; needed if using a dark background
    for i in /home/${USER}/.nano/syntax/*.nanorc; do sed -i "s|black|magenta|g" "${i}" ; done
    for i in /home/${USER}/.nano/syntax/*.nanorc; do sed -i "s|brightwhite,cyan|red,yellow|g" "${i}" ; done
    printf "\n  \e[0;33mChanged syntax highlighting to go well on top of a dark background\e[0m\n"
  elif [[ "${response}" =~ ^([nN][oO]|[nN])+$ ]] ; then
    # answered no
    true
  elif [[ ! "${response}" =~ ^([yY][eE][sS]|[nN][oO]|[yY]|[nN])+$ ]] ; then
    # unacceptable answer, ask again
    ask_q_oldnano2
  fi
}

ask_q_oldnano1() {
  echo
  read -r -p "${oldnano1}" response
  echo
  if [[ "${response}" =~ ^([yY][eE][sS]|[yY])+$ ]] ; then
    # answered yes
    git clone git@gitlab.com:tomcharter365/old-nanorc.git
    cd old-nanorc/nanorc
    make install
    for i in /home/${USER}/.nano/syntax/*.nanorc; do echo "include $i" >> /home/${USER}/.nanorc ; done
    sed -i '/ALL.nanorc/d' /home/${USER}/.nanorc
    ask_q_oldnano2
  elif [[ "${response}" =~ ^([nN][oO]|[nN])+$ ]] ; then
    # answered no
    printf "\n  \e[0;33mSkipping install of scopatz's nanorc\e[0m\n\n"
  elif [[ ! "${response}" =~ ^([yY][eE][sS]|[nN][oO]|[yY]|[nN])+$ ]] ; then
    # unacceptable answer, ask again
    ask_q_oldnano1
  fi
}

# =====================================
# Script
# =====================================
mkdir /home/${USER}/shell-color-tmp
cd /home/${USER}/shell-color-tmp/

ask_q_oldnano1

## Cleanup
rm -rf /home/${USER}/shell-color-tmp

## Announce success
printf "\n  \e[0;32minstall_old-nanorc.sh has finished.\e[0m\n\n"

